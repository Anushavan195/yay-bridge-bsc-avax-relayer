package admin

import (
	"context"
	"encoding/json"
	"fmt"
	pb "github.com/binance-chain/bsc-eth-swap/proto"
	"github.com/ethereum/go-ethereum/common"
	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
	"io/ioutil"
	"math/big"
	"net/http"
	"os"
	"strings"
	"time"

	cmm "github.com/binance-chain/bsc-eth-swap/common"
	"github.com/binance-chain/bsc-eth-swap/model"
	"github.com/binance-chain/bsc-eth-swap/util"
)

const (
	DefaultListenAddr = "0.0.0.0:8080"

	MaxIconUrlLength = 400
)

type Admin struct {
	DB         *gorm.DB
	cfg        *util.Config
	hmacSigner *util.HmacSigner
	swapEngine pb.SwapClient
	ctx        context.Context
}

func NewAdmin(ctx context.Context, config *util.Config, db *gorm.DB, signer *util.HmacSigner, swapEngine pb.SwapClient) *Admin {
	return &Admin{
		DB:         db,
		cfg:        config,
		hmacSigner: signer,
		swapEngine: swapEngine,
		ctx:        ctx,
	}
}

func updateCheck(update *updateSwapPairRequest) error {
	if update.ERC20Addr == "" {
		return fmt.Errorf("bsc_token_contract_addr can't be empty")
	}
	if update.UpperBound != "" {
		if _, ok := big.NewInt(0).SetString(update.UpperBound, 10); !ok {
			return fmt.Errorf("invalid upperBound amount: %s", update.UpperBound)
		}
	}
	if update.LowerBound != "" {
		if _, ok := big.NewInt(0).SetString(update.LowerBound, 10); !ok {
			return fmt.Errorf("invalid lowerBound amount: %s", update.LowerBound)
		}
	}
	if len(update.IconUrl) > MaxIconUrlLength {
		return fmt.Errorf("icon length exceed limit")
	}
	return nil
}

func (admin *Admin) UpdateSwapPairHandler(w http.ResponseWriter, r *http.Request) {
	reqBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	// reqBody, err := admin.checkAuth(r)

	var updateSwapPair updateSwapPairRequest
	err = json.Unmarshal(reqBody, &updateSwapPair)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	if err := updateCheck(&updateSwapPair); err != nil {
		http.Error(w, fmt.Sprintf("parameters is invalid, %v", err), http.StatusBadRequest)
		return
	}

	swapPair := model.SwapPair{}
	err = admin.DB.Where("erc20_addr = ?", updateSwapPair.ERC20Addr).First(&swapPair).Error
	if err != nil {
		http.Error(w, fmt.Sprintf("swapPair %s is not found", updateSwapPair.ERC20Addr), http.StatusBadRequest)
		return
	}

	toUpdate := map[string]interface{}{
		"available": updateSwapPair.Available,
	}

	if updateSwapPair.LowerBound != "" {
		toUpdate["low_bound"] = updateSwapPair.LowerBound
	}
	if updateSwapPair.UpperBound != "" {
		toUpdate["upper_bound"] = updateSwapPair.UpperBound
	}
	if updateSwapPair.IconUrl != "" {
		toUpdate["icon_url"] = updateSwapPair.IconUrl
	}

	err = admin.DB.Model(model.SwapPair{}).Where("erc20_addr = ?", updateSwapPair.ERC20Addr).Updates(toUpdate).Error
	if err != nil {
		http.Error(w, fmt.Sprintf("update swapPair error, err=%s", err.Error()), http.StatusInternalServerError)
		return
	}

	// get swapPair
	swapPair = model.SwapPair{}
	err = admin.DB.Where("erc20_addr = ?", updateSwapPair.ERC20Addr).First(&swapPair).Error
	if err != nil {
		http.Error(w, fmt.Sprintf("swapPair %s is not found", updateSwapPair.ERC20Addr), http.StatusBadRequest)
		return
	}

	swapPairIns, err := admin.swapEngine.GetSwapPairInstance(admin.ctx, &pb.GetSwapPairRequest{Erc20Addr: updateSwapPair.ERC20Addr})
	// disable is only for frontend, do not affect backend
	// if we want to disable it in backend, set the low_bound and upper_bound to be zero
	tempSwapPair := &pb.SwapPairRequest{
		Sponsor:    swapPair.Sponsor,
		Symbol:     swapPair.Symbol,
		Name:       swapPair.Name,
		Decimals:   int64(swapPair.Decimals),
		BEP20Addr:  swapPair.BEP20Addr,
		ERC20Addr:  swapPair.ERC20Addr,
		Available:  swapPair.Available,
		LowBound:   swapPair.LowBound,
		UpperBound: swapPair.UpperBound,
		IconUrl:    swapPair.IconUrl,
	}

	if err != nil && updateSwapPair.Available {
		// add swapPair in swapper
		var _, err = admin.swapEngine.AddSwapPairInstance(admin.ctx, tempSwapPair)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

	} else if swapPairIns != nil {
		tempSwapPair.RecordHash = swapPair.RecordHash
		_, err := admin.swapEngine.UpdateSwapInstance(admin.ctx, tempSwapPair)
		if err != nil {
			panic(err.Error())
		}
	}

	jsonBytes, err := json.MarshalIndent(swapPair, "", "  ")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)

	_, err = w.Write(jsonBytes)
	if err != nil {
		util.Logger.Errorf("write response error, err=%s", err.Error())
	}
}

func (admin *Admin) Endpoints(w http.ResponseWriter, r *http.Request) {
	endpoints := struct {
		Endpoints []string `json:"endpoints"`
	}{
		Endpoints: []string{
			"/",
			// 			"/update_swap_pair",
			// 			"/healthz",
		},
	}

	jsonBytes, err := json.MarshalIndent(endpoints, "", "    ")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	_, err = w.Write(jsonBytes)
	if err != nil {
		util.Logger.Errorf("write response error, err=%s", err.Error())
	}
}

func (admin *Admin) WithdrawToken(w http.ResponseWriter, r *http.Request) {
	reqBody, err := admin.checkAuth(r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	var withdrawToken withdrawTokenRequest
	err = json.Unmarshal(reqBody, &withdrawToken)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	if err = withdrawCheck(&withdrawToken); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	amount := big.NewInt(0)
	amount.SetString(withdrawToken.Amount, 10)

	var withdrawResp withdrawTokenResponse
	result, err := admin.swapEngine.WithdrawToken(admin.ctx, &pb.WithdrawTokenRequest{Chain: withdrawToken.Chain, TokenAddr: withdrawToken.TokenAddr, Recipient: withdrawToken.Recipient, Amount: amount.Bytes()})
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	withdrawResp.TxHash = result.Hash
	withdrawResp.ErrMsg = err.Error()
	jsonBytes, err := json.MarshalIndent(withdrawResp, "", "    ")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	_, err = w.Write(jsonBytes)
	if err != nil {
		util.Logger.Errorf("write response error, err=%s", err.Error())
	}

}

func withdrawCheck(withdraw *withdrawTokenRequest) error {
	if strings.ToUpper(withdraw.Chain) != cmm.ChainBSC && strings.ToUpper(withdraw.Chain) != cmm.ChainETH {
		return fmt.Errorf("bsc_token_contract_addr can't be empty")
	}
	if !common.IsHexAddress(withdraw.TokenAddr) {
		return fmt.Errorf("token address is not a valid address")
	}
	if !common.IsHexAddress(withdraw.Recipient) {
		return fmt.Errorf("recipient is not a valid address")
	}
	_, ok := big.NewInt(0).SetString(withdraw.Amount, 10)
	if !ok {
		return fmt.Errorf("invalid input, expected big integer")
	}
	return nil
}

func (admin *Admin) RetryFailedSwaps(w http.ResponseWriter, r *http.Request) {
	reqBody, err := admin.checkAuth(r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	var retryFailedSwaps retryFailedSwapsRequest
	err = json.Unmarshal(reqBody, &retryFailedSwaps)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	var retryFailedSwapsResp retryFailedSwapsResponse
	result, err := admin.swapEngine.InsertRetryFailedSwaps(admin.ctx, &pb.InsertRetryFailedSwapsRequest{SwapIDList: retryFailedSwaps.SwapIDList})
	retryFailedSwapsResp.SwapIDList = result.List
	retryFailedSwapsResp.RejectedSwapIDList = result.RejectedList

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		retryFailedSwapsResp.ErrMsg = err.Error()
	} else {
		w.WriteHeader(http.StatusOK)
	}

	jsonBytes, err := json.MarshalIndent(retryFailedSwapsResp, "", "    ")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")

	_, err = w.Write(jsonBytes)
	if err != nil {
		util.Logger.Errorf("write response error, err=%s", err.Error())
	}
}

func (admin *Admin) Healthz(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
}

func (admin *Admin) checkAuth(r *http.Request) ([]byte, error) {
	//apiKey := r.Header.Get("ApiKey")
	//hash := r.Header.Get("Authorization")

	payload, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return nil, err
	}

	//if admin.hmacSigner.ApiKey != apiKey {
	//	return nil, fmt.Errorf("api key mismatch")
	//}

	//if !admin.hmacSigner.Verify(payload, hash) {
	//	return nil, fmt.Errorf("invalud auth")
	//}
	return payload, nil
}

func (admin *Admin) Serve() {
	router := mux.NewRouter()

	router.HandleFunc("/", admin.Endpoints).Methods("GET")
	router.HandleFunc("/healthz", admin.Healthz).Methods("GET")
	// 	router.HandleFunc("/update_swap_pair", admin.UpdateSwapPairHandler).Methods("PUT")
	// 	router.HandleFunc("/withdraw_token", admin.WithdrawToken).Methods("POST")
	//	router.HandleFunc("/retry_failed_swaps", admin.RetryFailedSwaps).Methods("POST")
	listenAddr := DefaultListenAddr
	if admin.cfg.AdminConfig.ListenAddr != "" {
		listenAddr = admin.cfg.AdminConfig.ListenAddr
	}
	port := os.Getenv("PORT")
	if port != "" {
		listenAddr = ":" + port
	}
	srv := &http.Server{
		Handler:      router,
		Addr:         listenAddr,
		WriteTimeout: 3 * time.Second,
		ReadTimeout:  3 * time.Second,
	}

	util.Logger.Infof("start admin server at %s", srv.Addr)

	err := srv.ListenAndServe()
	if err != nil {
		panic(fmt.Sprintf("start admin server error, err=%s", err.Error()))
	}
}
