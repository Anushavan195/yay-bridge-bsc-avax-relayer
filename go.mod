module github.com/binance-chain/bsc-eth-swap

go 1.14

require (
	github.com/aws/aws-sdk-go v1.34.21
	github.com/btcsuite/btcd v0.20.1-beta // indirect
	github.com/ethereum/go-ethereum v1.9.12
	github.com/go-logfmt/logfmt v0.5.0 // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/gorilla/mux v1.8.0
	github.com/hashicorp/golang-lru v0.5.4 // indirect
	github.com/jinzhu/gorm v1.9.16
	github.com/jinzhu/now v1.1.2 // indirect
	github.com/lib/pq v1.10.2 // indirect
	github.com/mattn/go-colorable v0.1.6 // indirect
	github.com/mattn/go-sqlite3 v2.0.1+incompatible // indirect
	github.com/op/go-logging v0.0.0-20160315200505-970db520ece7
	github.com/spf13/pflag v1.0.3
	github.com/spf13/viper v1.6.3
	github.com/stretchr/testify v1.7.0 // indirect
	github.com/tendermint/tendermint v0.32.3
	golang.org/x/crypto v0.0.0-20210921155107-089bfa567519 // indirect
	golang.org/x/text v0.3.7 // indirect
	google.golang.org/grpc v1.32.0
	google.golang.org/protobuf v1.27.1
	gopkg.in/natefinch/lumberjack.v2 v2.0.0
)
