package swap

import (
	"crypto/ecdsa"
	"math/big"
	"sync"

	"github.com/ethereum/go-ethereum/accounts/abi"

	ethcom "github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/ethclient"
	"github.com/jinzhu/gorm"

	"github.com/binance-chain/bsc-eth-swap/util"
)

type SwapEngine struct {
	mutex    sync.RWMutex
	db       *gorm.DB
	hmacCKey string
	config   *util.Config
	// key is the bsc contract addr
	swapPairsFromERC20Addr map[ethcom.Address]*SwapPairIns
	ethClient              *ethclient.Client
	bscClient              *ethclient.Client
	ethPrivateKey          *ecdsa.PrivateKey
	bscPrivateKey          *ecdsa.PrivateKey
	ethChainID             int64
	bscChainID             int64
	bep20ToERC20           map[ethcom.Address]ethcom.Address
	erc20ToBEP20           map[ethcom.Address]ethcom.Address

	ethSwapAgentABI *abi.ABI
	bscSwapAgentABI *abi.ABI

	ethSwapAgent ethcom.Address
	bscSwapAgent ethcom.Address
}

type SwapPairEngine struct {
	mutex   sync.RWMutex
	db      *gorm.DB
	hmacKey string
	config  *util.Config

	swapEngine *SwapEngine

	bscClient       *ethclient.Client
	bscPrivateKey   *ecdsa.PrivateKey
	bscChainID      int64
	bscTxSender     ethcom.Address
	bscSwapAgent    ethcom.Address
	bscSwapAgentABi *abi.ABI
}

type SwapPairIns struct {
	Symbol     string
	Name       string
	Decimals   int
	LowBound   *big.Int
	UpperBound *big.Int

	BEP20Addr ethcom.Address
	ERC20Addr ethcom.Address
}
