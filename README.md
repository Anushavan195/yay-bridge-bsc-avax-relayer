# BSC-AVAX-SWAP

## Build

```shell script
make build
```

## Configuration

1. Generate BSC private key and AVAX private key.

2. Transfer enough BNB and AVAX to the above two accounts.

3. Config swap agent contracts

   1. Deploy contracts in [token-bridge-contracts](https://github.com/YAY-Games/token-bridge-contracts)
   2. Example deployed contracts on testnet please refer to [BSCSwapAgent](https://testnet.bscscan.com/address/0x4e8a981fEe6f67731F6EB6F981d801497f38b0Ca#code) and [AVAXSwapAgent](https://cchain.explorer.avax-test.network/address/0xC42749753E4d199E3e99b02F1de0fa9d361DeBe0/)
   3. Write the two contract address to `bsc_swap_agent_addr` and `avax_swap_agent_addr`.
   4. Tip: in this config version ETH=BSC, BSC=AVAX

4. Config start height
   
   Get the latest height for both BSC and ETH, and write them to `bsc_start_height` and `avax_swap_agent_addr`.

## Start

```shell script
./build/swap-backend --config-type local --config-path config/config.json
```

or

```shell script
.start.sh
```

## Specification

Refer to [specification](./docs/README.md)
